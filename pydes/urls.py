"""pydes URL Configuration"""
from django.contrib import admin
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from pydes.api import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/status', views.StatusView.as_view(), name='status')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
