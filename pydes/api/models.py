from django.db import models


class License(models.Model):
    bot_license = models.CharField(primary_key=True, max_length=255)
    last_ip = models.CharField(max_length=100, null=True)
    last_date = models.DateTimeField(auto_now_add=True)
    notes = models.TextField(null=True)
    owner = models.CharField(max_length=100)
    active = models.BooleanField(default=True)

    def __str__(self):
        return self.bot_license


class Status(models.Model):
    bot_license = models.ForeignKey('License', on_delete=models.CASCADE)
    pair = models.CharField(max_length=20)
    status = models.CharField(max_length=100)
    simulation = models.BooleanField(default=True)
    funds = models.FloatField(default=0.0)
    last_price = models.FloatField(null=True)
    stop_loss_order = models.IntegerField(null=True)
    take_profit_order = models.IntegerField(null=True)

    class Meta:
        unique_together = ('bot_license', 'pair', 'simulation')
