from rest_framework import response
from rest_framework import status
from rest_framework import views

from pydes.api import permissions
from pydes.api import models
from pydes.api import serializers


class StatusView(views.APIView):
    """Update the status given the pair, license and simulation"""
    permissions_classes = (permissions.PydesPermission,)

    def post(self, request):
        bot_pair = request.data['pair']
        bot_simulation = request.data['simulation']
        bot_license = request.META['HTTP_LICENSE']
        bot_status = request.data['status']
        bot_funds = request.data.get('funds', None)
        bot_last_price = request.data.get('bot_last_price', None)
        bot_stop_loss_order = request.data.get('stop_loss_order', None)
        bot_take_profit_order = request.data.get('take_profit_order', None)
        try:
            status_obj = models.Status.objects.filter(
                bot_license=bot_license,
                pair=bot_pair,
                simulation=get_boolean(bot_simulation)
            ).get()
            status_obj.status = bot_status
            status_obj.last_price = bot_last_price
            status_obj.stop_loss_order = bot_stop_loss_order
            status_obj.take_profit_order = bot_take_profit_order
            if bot_funds:
                status_obj.funds = float(bot_funds)
            status_obj.save()
            serializer = serializers.StatusSerializer(status_obj)
            return response.Response(serializer.data)
        except models.Status.DoesNotExist:
            return response.Response(status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        try:
            bot_license = request.META['HTTP_LICENSE']
            data = {'bot_license': bot_license,
                    'pair': request.query_params['pair'],
                    'simulation': get_boolean(request.query_params['simulation'])}
            bot_license = models.License.objects.get(pk=bot_license)
            bot_status, created = models.Status.objects.get_or_create(
                bot_license=bot_license,
                pair=data['pair'],
                simulation=data['simulation'],
            )
        except Exception:
            return response.Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = serializers.StatusSerializer(bot_status, data=data)
            if serializer.is_valid():
                serializer.save()
                return response.Response(serializer.data,
                                         status=status.HTTP_200_OK)
            else:
                return response.Response(serializer.errors,
                                         status=status.HTTP_400_BAD_REQUEST)


def get_boolean(var):
    if var.lower() == 'false':
        return False
    return True
