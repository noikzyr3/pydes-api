from django.contrib import admin

from pydes.api import models


class LicenseAdmin(admin.ModelAdmin):
    list_display = ('bot_license', 'last_ip', 'last_date', 'notes', 'owner', 'active')


class StatusAdmin(admin.ModelAdmin):
    list_display = ('bot_license', 'pair', 'status', 'simulation', 'funds')

admin.site.register(models.Status)
admin.site.register(models.License, LicenseAdmin)
