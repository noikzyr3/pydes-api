# Generated by Django 2.0.4 on 2018-04-21 09:55

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20180408_2147'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='status',
            unique_together={('bot_license', 'pair', 'simulation')},
        ),
    ]
