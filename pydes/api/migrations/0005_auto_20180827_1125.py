# Generated by Django 2.0.4 on 2018-08-27 11:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_auto_20180421_0955'),
    ]

    operations = [
        migrations.AddField(
            model_name='status',
            name='last_price',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='status',
            name='stop_loss_order',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='status',
            name='take_profit_order',
            field=models.IntegerField(null=True),
        ),
    ]
