from django.test import TestCase

from pydes.api import models


class ApiTestCase(TestCase):
    def create_license(self):
        lic = models.License(bot_license='ABCDEFGHIJK1234567890',
                             owner='test')
        lic.save()
        return lic
