from django.urls import reverse

from rest_framework import status

from pydes.api.tests import base
from pydes.api import models


class StatusTestCase(base.ApiTestCase):
    def setUp(self):
        self.url = reverse('status')
        self.lic = self.create_license()

    def test_status_get__good(self):
        params = {'pair': 'XRPETH',
                  'simulation': '1'}
        response = self.client.get(self.url, params, HTTP_LICENSE=self.lic.bot_license)
        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(models.Status.objects.all().count(), 1)

    def test_status_get__bad(self):
        params = {'simulation': '1'}
        response = self.client.get(self.url, params, HTTP_LICENSE=self.lic.bot_license)
        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
        self.assertEqual(models.Status.objects.all().count(), 0)

    def test_status_post__good(self):
        bot_status = models.Status(bot_license=self.lic,
                                   pair='XRPETH',
                                   status='one status',
                                   simulation=True,
                                   funds=0.2)
        bot_status.save()
        data = {'pair': bot_status.pair,
                'simulation': '1',
                'status': 'other status',
                'funds': 0.21}
        response = self.client.post(self.url, data=data, HTTP_LICENSE=self.lic.bot_license)
        new_status = models.Status.objects.all().first()

        self.assertEqual(response.status_code,
                         status.HTTP_200_OK)
        self.assertEqual(new_status.status, data['status'])
        self.assertTrue(new_status.simulation)
        self.assertEqual(new_status.funds, data['funds'])

    def test_status_post__bad(self):
        bot_status = models.Status(bot_license=self.lic,
                                   pair='XRPETH',
                                   status='one status',
                                   simulation=True,
                                   funds=0.2)
        bot_status.save()
        data = {'pair': 'meh',
                'simulation': '1',
                'status': 'other status',
                'funds': 0.21}
        response = self.client.post(self.url, data=data, HTTP_LICENSE=self.lic.bot_license)

        self.assertEqual(response.status_code,
                         status.HTTP_400_BAD_REQUEST)
