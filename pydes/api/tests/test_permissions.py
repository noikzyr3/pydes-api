import datetime

import mock

from pydes.api.tests import base

from pydes.api import permissions


class permissionTestCase(base.ApiTestCase):
    def setUp(self):
        self.permission = permissions.PydesPermission()
        self.request = MockRequest()

    def test_get_license__false(self):
        self.assertFalse(self.permission.get_license('whatever'))

    def test_get_license__none(self):
        lic = self.create_license()
        self.assertTrue(self.permission.get_license(lic.bot_license))

    def test_check_time__must_check_ip__different_ip(self):
        lic = self.create_license()
        lic.last_ip = '127.0.0.1'
        lic.save()

        self.assertFalse(self.permission.check_time(lic, '123.1.1.3'))
        self.assertFalse(lic.active)

    def test_check_time__must_check_ip__same_ip(self):
        lic = self.create_license()
        lic.last_ip = '127.0.0.1'
        lic.save()

        self.assertTrue(self.permission.check_time(lic, lic.last_ip))
        self.assertTrue(lic.active)

    def test_check_time__must_check_ip__empty_ip(self):
        lic = self.create_license()
        new_ip = '123.1.1.3'
        self.assertTrue(self.permission.check_time(lic, new_ip))
        self.assertTrue(lic.active)
        self.assertEqual(new_ip, lic.last_ip)

    def test_check_time__no_check_ip(self):
        lic = self.create_license()
        new_ip = '123.1.1.3'
        lic.last_date -= datetime.timedelta(hours=1)

        self.assertTrue(self.permission.check_time(lic, new_ip))
        self.assertEqual(new_ip, lic.last_ip)
        self.assertTrue(lic.active)

    @mock.patch('pydes.api.permissions.PydesPermission.check_time', autospec=True)
    def test_has_permission__not_license(self, m_check_time):
        self.request.META['headers'] = {'license': 'whatever'}
        m_check_time.return_value = True

        self.assertFalse(self.permission.has_permission(self.request, mock.Mock))

    @mock.patch('pydes.api.permissions.PydesPermission.check_time', autospec=True)
    def test_has_permission__check_time_fail(self, m_check_time):
        lic = self.create_license()
        self.request.META['headers'] = {'license': lic.bot_license}
        m_check_time.return_value = False

        self.assertFalse(self.permission.has_permission(self.request, mock.Mock))

    @mock.patch('pydes.api.permissions.PydesPermission.check_time', autospec=True)
    def test_has_permission__true(self, m_check_time):
        lic = self.create_license()
        self.request.META['headers'] = {'license': lic.bot_license}
        m_check_time.return_value = True

        self.assertTrue(self.permission.has_permission(self.request, mock.Mock))


class MockRequest(object):
    def __init__(self):
        self.META = {'REMOTE_ADDR': '123.0.0.3'}
        self.data = {}
