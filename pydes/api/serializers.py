from rest_framework import serializers

from pydes.api import models


class LicenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.License
        fields = '__all__'


class StatusSerializer(serializers.ModelSerializer):
    status = serializers.CharField(required=False)

    class Meta:
        model = models.Status
        fields = '__all__'

    def create(self, validated_data):
        models.Status.objects.update_or_create(
            status=validated_data.get('status'),
            bot_license=validated_data.get('bot_license'),
            pair=validated_data.get('pair'),
            simulation=validated_data.get('simulation')
        )
