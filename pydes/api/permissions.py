from datetime import datetime
from datetime import timedelta

from rest_framework import permissions

from pydes.api import models


class PydesPermission(permissions.BasePermission):
    """Return True if there is an authenticated user.

    The user in request is set in api.authentication.PotterAuthentication
    that only happens if the auth is good.
    """

    def has_permission(self, request, view):
        bot_license = request.META['HTTP_LICENSE']
        bot_ip = request.META['REMOTE_ADDR']

        # Check Licence
        bot_license_obj = self.get_license(bot_license)
        if not bot_license_obj:
            return False

        # Check Time and IP
        if not self.check_time(bot_license_obj, bot_ip):
            return False

        return True

    def get_license(self, bot_license):
        try:
            return models.License.objects.get(bot_license=bot_license, active=True)
        except models.License.DoesNotExist:
            return False

    def check_time(self, lic, bot_ip):
        now = datetime.now(lic.last_date.tzinfo)
        if lic.last_date > now - timedelta(seconds=60):
            lic.last_date = now
            if lic.last_ip != bot_ip and lic.last_ip is not None:
                lic.active = False
                lic.save()
                return False
        lic.last_date = now
        lic.last_ip = bot_ip
        lic.save()
        return True

